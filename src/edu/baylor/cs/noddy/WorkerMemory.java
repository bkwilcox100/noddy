package edu.baylor.cs.noddy;

import java.util.Set;

final public class WorkerMemory extends Worker implements Cloneable {

	
	public WorkerMemory() {
		super();
	}
	public WorkerMemory(String name, Set<Worker> finishSet) {
		super(name, finishSet);
	}

	@Override
	public void run() {
		try {
			StringBuffer buf = new StringBuffer("SSSSSSSSSSSSSSSSSSSSSSSSSSSSSS");
			String s = "SSSSSSSSSSSSSSSSSSSSSSSSSSSSSS";
			if(true) {
				 for (int i=0 ; i<20 ; i++) {
			            buf.append(getName());
			     }
			}
			Thread.sleep(Integer.MAX_VALUE);

			getFinishSet().add(this);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

	}
	
	public Worker clone() {
		return new WorkerMemory();
	}

}
