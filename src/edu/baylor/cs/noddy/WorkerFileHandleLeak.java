package edu.baylor.cs.noddy;

import java.io.*;
import java.util.Set;
import java.util.zip.ZipOutputStream;
import java.util.zip.ZipEntry;;

final public class WorkerFileHandleLeak extends Worker implements Cloneable {


	public WorkerFileHandleLeak() {
		super();
	}
	public WorkerFileHandleLeak(String name, Set<Worker> finishSet) {
		super(name, finishSet);
	}

	@Override
	public void run() {
		
		String name = getName()+"resource.txt";
		ZipOutputStream zos = null;
		try {
			FileOutputStream in = new FileOutputStream(new File(getName()+"big.jar"));
			zos = new ZipOutputStream(in);
			zos.putNextEntry(new ZipEntry(name));
	        zos.write("not too much in here".getBytes("UTF-8"));
	        zos.closeEntry();
	        zos.putNextEntry(new ZipEntry(getName()+"largeFile.out"));
	        for (int i=0 ; i<10000000 ; i++) {
	            zos.write((int) (Math.round(Math.random()*100)+20));
	        }
	        zos.closeEntry();
			in.close();
			zos.close();

	        
	        // leak - resource never closes
	        int ITERATIONS=2000;
	        for (int i=0 ; i<ITERATIONS ; i++) {
	        	WorkerFileHandleLeak.class.getClassLoader().getResourceAsStream("test.properties");
	        	WorkerFileHandleLeak.class.getClassLoader().getResource("test.properties");
	        	WorkerFileHandleLeak.class.getClassLoader().getResource("/cz/cvut/fel/cs/ass/test.properties");
	        	//new FileInputStream(new File(getName()+"big.jar"));

	        }
	        System.out.println("finished creation of streams, now waiting to be killed");

	        Thread.sleep(Long.MAX_VALUE);
	       // fix to the leak 
	       // WorkerMemory.class.getClassLoader().getResourceAsStream(name+"resource.txt").close();

			getFinishSet().add(this);
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());

		} catch (InterruptedException e2){
			System.err.println(e2.getMessage());
		} catch (IOException e3){
			System.err.println(e3.getMessage());
		} finally {
			if (zos != null){
				try {
					zos.close();
				} catch (IOException e){
					e.printStackTrace();
				}
			}
		}

	}
	
	public Worker clone() {
		return new WorkerFileHandleLeak();
	}

}
