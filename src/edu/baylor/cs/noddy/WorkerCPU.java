package edu.baylor.cs.noddy;

import java.util.Set;

final public class WorkerCPU extends Worker implements Cloneable {


	public WorkerCPU() {
		super();
	}
	public WorkerCPU(String name, Set<Worker> finishSet) {
		super(name, finishSet);
	}

	@Override
	public void run() {
		try {
			if(true) {
				for (;;) {
				}
			}
			getFinishSet().add(this);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

	}

	public Worker clone() {
		return new WorkerCPU();
	}

}
